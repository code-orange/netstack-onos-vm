#!/bin/bash
set -e

if [ -d "/opt/onos" ]; then
  rm -rf /opt/onos
fi

wget https://repo1.maven.org/maven2/org/onosproject/onos-releases/2.3.0/onos-2.3.0.tar.gz -O /tmp/onos_bin.tar.gz
mkdir /opt/onos
tar -xvf /tmp/onos_bin.tar.gz --strip 1 -C /opt/onos

cp /opt/onos/init/onos.initd /etc/init.d/onos
cp /opt/onos/init/onos.service /etc/systemd/system/onos.service

systemctl daemon-reload
